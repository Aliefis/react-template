import React from 'react'
import {Link} from 'react-router-dom'

function Header(){
    return(
        <nav className="navbar navbar-dark bg-dark">
            <div className="navbar-brand">AXIOS</div>
            <div className="navbar-nav-scroll">
                    <ul class="navbar-nav bd-navbar flex-row">
                        <li class="nav-item active">
                            <Link to="/" className="nav-link">Home</Link>
                        </li>
                        <li class="nav-item">
                            <Link to="/add" className="nav-link">Tambah Data</Link>
                        </li>
                    </ul>
                </div>
        </nav>
    )
}

export default Header
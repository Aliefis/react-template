import React, {Component} from 'react'
import axios from 'axios'
import BsAlert from '../utils/BsAlert'

export class DaftarData extends Component{

    state={
        data:[],
    }

    async componentDidMount(){
        const url = "https://jsonplaceholder.typicode.com/users"
        await axios.get(url)
        .then(response=>this.setState({
            data:response.data
        }))

    }

    render(){
        const renderData = this.state.data.map(data=>{
            return(
                <BsAlert data={data}></BsAlert>
                
            )
        })
        return(
            <div>
                <h3>Daftar Data</h3>
                {renderData}
            </div>
        )
    }
}

export default DaftarData
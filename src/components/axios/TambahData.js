import React, { Component } from 'react'
import axios from 'axios'

export default class TambahData extends Component {

    state={
        name : "",
        job : "Warrior",
    }


    handlerButton =()=> {
        const name = document.getElementById("name").value
        this.setState({ // remember set state is async
            name : name
        }, ()=>
        this.postData()
        )

    }

    postData =async()=>{
        const url = "https://reqres.in/api/users"
        await axios.post(url,this.state).then((response) => {
            console.log(response);
          }, (error) => {
            console.log(error);
          });
    }

    printValue=()=>{
        console.log("print: " + this.state.name)
    }

    render() {

        return (
            <div class="container">
                <h3>Tambah Data</h3>

                <form>
                    <table>
                        <tr>
                            <td>Nama Pekerja</td>
                            <td><input type="text" id="name"></input></td>
                        </tr>
                    </table>
                    <button type="button" onClick={this.handlerButton}>Submit</button>
                </form>
            </div>
        )
    }
}

import React from 'react';

import {BrowserRouter,Route} from 'react-router-dom'
import Header from './axios/Header'

import DaftarData from './axios/DaftarData'
import TambahData from './axios/TambahData'


const AxiosTutorial =()=> {
    return (
        <BrowserRouter>
            <Header></Header>
            <div className="container mt-4">
                <Route exact path="/" component={DaftarData}></Route> 
                <Route path="/add" component={TambahData}></Route>
            </div>        
        </BrowserRouter>

    )
}

export default AxiosTutorial;

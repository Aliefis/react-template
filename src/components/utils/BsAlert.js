import React from 'react'

function BsAlert({data}) {
    return (
        <div key={data.id} id={'data_'+data.id} className="alert alert-secondary">
            <h6>{data.username}</h6>
            <div>{data.email}</div>
        </div>
    )
}

export default BsAlert

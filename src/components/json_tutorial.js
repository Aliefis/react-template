import React from 'react';
import ListUser from './json/ListUser'
import DetailUser from './json/DetailUser'
import {BrowserRouter,Route} from 'react-router-dom'

const Json_tutorial =()=> {
    return (
        <BrowserRouter>
            <div className="container">
                <h1>JSON Tutorial</h1>
                <Route exact path="/" component={ListUser}></Route> 
                <Route path="/detailuser/:id_user" component={DetailUser}></Route>{/* capture id_user */}
            </div>        
        </BrowserRouter>

    )
}

export default Json_tutorial;
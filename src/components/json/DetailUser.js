import React, {Component} from 'react';

class DetailUser extends Component {

    state = {
        user: null,
        loading: true,
    }

    componentDidMount() { // fetch data from json based on id
        const id = this.props.match.params.id_user; // fetch id user from param
        fetch('https://jsonplaceholder.typicode.com/users/'+id)
        .then(response=>{
            if(response.ok){
                return response.json(); // if success then return json
            } else {
                throw new Error('Ambil Data Gagal') // throw error if failed
            }
        })
        .then(data=>this.setState({ // set the state 
            user: data,
            loading: false,
        }))
    }

    render(){
        const {loading,user} = this.state 

        if (loading) { // if loading
            return(
                <div>
                    Loading....
                </div>
            )
        }
        
        return(
            <div className="container">
                <h3>Detail User</h3>
                <div className="alert alert-success">
                    <h3>{user.name}</h3>
                    <div>{user.email}</div>
                    <div>{user.username}</div>
                </div>
                
            </div>
        )
    }
}

export default DetailUser;
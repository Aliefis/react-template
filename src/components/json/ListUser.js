import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class ListUser extends Component {
    
    state = {
        datauser : [],
        loading : true,
        error : false,
    }

    componentDidMount(){
        // Run this code after 
        fetch('https://jsonplaceholder.typicode.com/users') // Fetch the json data
        .then(response=>{
            if(response.ok){
                return response.json(); // if success then return json
            } else {
                throw new Error('Ambil Data Gagal') // throw error if failed
            }
        })
        .then(
            data=>this.setState({ 
                    datauser : data,
                    loading : false, 
                })
        )
        .catch(error=>this.state({ // if error from that else occurred
            error:error,
            loading:true,
        }))
        
    }

    render() {

        const { error,loading,datauser } = this.state // Equal as const error = this.state.error, const loading = this.state.loading etc
        
        if (error) { // if error, print error message
            return(
                <div>
                    {error.message}
                </div>
            )
        }

        if (loading) { // if loading
            return(
                <div>
                    Loading....
                </div>
            )
        }

        const listuser = datauser.map(user=>{ // map the data
            return(
                <Link to={'/detailuser/' + user.id}>
                    <div key={user.id} id={user.id} className="alert alert-secondary font-weight-bold">{user.name}</div>
                </Link>
                
            )
        })

        return(
            <div>
                {listuser}
            </div>
        )
    }
}

export default ListUser;
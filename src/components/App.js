import React from 'react';

import {BrowserRouter,Route} from 'react-router-dom'

// Template Data

const App =()=> {
    return (
        <BrowserRouter>
            <div className="container">
                <h1>App Class</h1>
                <Route exact path="/"></Route> 
            </div>        
        </BrowserRouter>

    )
}

export default App;

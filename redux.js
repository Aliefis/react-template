// REDUX Global State

const redux = require('redux') // requirements
const createStore = redux.createStore // create store variable

const initialState = { // objects as initial value
    value : 0,
    age : 17

}

// Reducer (Function to alter the state value)
const rootReducer =(state = initialState, action)=> {

    // Using SWITCH CASE function 
    switch (action.type){

        case "ADD_AGE":
            console.log("ADD_AGE EXECUTED")
            return {
                ...state, // copy the data (spread opertor)
                age: state.age + 1 // and then age + 1
            }

        case "CHANGE_VALUE":
            console.log("CHANGE_VALUE EXECUTED")
            return{
                ...state,
                value: state.value + action.newValue
            }

        default:
            return state
    }

    return state
}

// Store (Used to create initial state)
const store = createStore(rootReducer)
console.log("CREATING STORE: ",store.getState())

// Subscription (Function that will always called when store value is changed)
store.subscribe(()=> {
    console.log("Store Change: ",store.getState())
})

// Dispatching Action ( Call this function to alter the state via Reducer)
store.dispatch({ // calling ADD_AGE reducer function
    type: "ADD_AGE"
})

store.dispatch({ // calling CHANGE_VALUE reducer and alter the value function
    type: "CHANGE_VALUE",
    newValue: 12 
})

console.log(store.getState())

